package com.orest.shkliaruk;

public class GoldStuff extends Metal {

    public GoldStuff(double weight){
        this.setWeight(weight);
        System.out.println("Now you have an GoldStuff for "+this.calculatePrice
                (valueGoldStuff, this.weight)+" and weight "+this.weight);
    }
    private double weight;
    private int valueGoldStuff = 60;
    private String size;
    public double priceGoldStuff;

    public double calculatePrice(int value, double weight){
        return priceGoldStuff = weight*valueGoldStuff;
    }
    @Override
    public int getValueSilverStuff() {
        return valueGoldStuff;
    }

    @Override
    public void setValueSilverStuff(int valueSilverStuff) {
        this.valueGoldStuff = valueSilverStuff;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public void setWeight(double silverStuff) {
        this.weight = silverStuff;
    }

    @Override
    public String getType() {
        return size;
    }

    @Override
    public void setType(String type) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "GoldStuff";
    }


}
