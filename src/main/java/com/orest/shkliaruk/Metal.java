package com.orest.shkliaruk;

public class Metal extends Jewel {

    public final boolean isMetal = true;

    @Override
    public String toString() {
        return "Metal";
    }
}
