package com.orest.shkliaruk;

public class Stone extends Jewel{
    public final boolean isStone = true;

    @Override
    public String toString() {
        return "Stone";
    }
}
