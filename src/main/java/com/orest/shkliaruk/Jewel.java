package com.orest.shkliaruk;

public abstract class Jewel {
    private int value;
    private double weight;
    private String size;

    public void shine(){
        System.out.println("Shining bright like a "+ this.toString());
    }

    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public int getValueSilverStuff() {
        return value;
    }
    public void setValueSilverStuff(int valueSilverStuff) {
        this.value = valueSilverStuff;
    }
    public String getType() {
        return size;
    }
    public void setType(String size) {
        this.size = size;
    }

}
