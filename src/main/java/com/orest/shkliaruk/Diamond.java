package com.orest.shkliaruk;

public class Diamond extends Jewel{
    public Diamond(double weight){
        this.setWeight(weight);
        System.out.println("Now you have an Diamond for "+this.calculatePrice
                (valueDiamond, this.weight)+" and weight "+this.weight);
    }

    private double weight;
    public double price;
    private String size;
    private int valueDiamond = 50;

    public double calculatePrice(int valueDiamond, double weight){
        return price = weight*valueDiamond;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double silverStuff) {
        this.weight = silverStuff;
    }

    public int getValueSilverStuff() {
        return valueDiamond;
    }

    public void setValueSilverStuff(int valueSilverStuff) {
        this.price = valueSilverStuff;
    }

    @Override
    public String toString() {
        return "Diamond";
    }

}
