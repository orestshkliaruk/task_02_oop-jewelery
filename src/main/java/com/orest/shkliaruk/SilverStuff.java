package com.orest.shkliaruk;

public class SilverStuff extends Metal{


    public SilverStuff(double weight){
        this.setWeight(weight);
        System.out.println("Now you have a SilverStuff for "+this.calculatePrice
                (valueSilverStuff, this.weight)+" and weight "+this.weight);
    }

    private double weight;
    private int valueSilverStuff = 60;
    private String size;
    public double priceSilverStuff;

    public double calculatePrice(int value, double weight){
        return priceSilverStuff = weight*valueSilverStuff;
    }

    @Override
    public int getValueSilverStuff() {
        return valueSilverStuff;
    }

    @Override
    public void setValueSilverStuff(int valueSilverStuff) {
        this.valueSilverStuff = valueSilverStuff;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String getType() {
        return size;
    }

    @Override
    public void setType(String type) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "SilverStuff";
    }

}
